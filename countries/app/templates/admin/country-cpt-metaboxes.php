<div class="countries_meta_information">
	<!-- Show Country Name Field -->
	<div class="flex items-center mb-1-rem">
		<label for="country_name" class="w-20-%"><?php _e( 'Country', 'countries' ); ?></label>
		<select name="country_name" id="country_name" class="w-80-%">
			<option>Select Country</option>
			<?php if ( ! empty( $country_names ) ) : ?>
				<?php foreach ( $country_names as $country ) : ?>
					<option value="<?php echo esc_attr( $country ); ?>" <?php selected( $country_name, $country, true ); ?>><?php echo esc_html( $country ); ?></option>
				<?php endforeach; ?>
			<?php endif; ?>
		</select>
	</div>

	<!-- Show Top Level Domain Field -->
	<div class="flex items-center mb-1-rem">
		<label for="top_level_domain" class="w-20-%"><?php _e( 'Top Level Domain', 'countries' ); ?></label>
		<input type="text" readonly name="top_level_domain" id="top_level_domain" class="w-80-%" value="<?php echo esc_attr( $top_level_domain ); ?>" />
	</div>

	<!-- Show Alpha2 Code Field -->
	<div class="flex items-center mb-1-rem">
		<label for="alpha2_code" class="w-20-%"><?php _e( 'Alpha2 Code', 'countries' ); ?></label>
		<input type="text" readonly name="alpha2_code" id="alpha2_code" class="w-80-%" value="<?php echo esc_attr( $alpha2_code ); ?>" />
	</div>

	<!-- Show Alpha3 Code Field -->
	<div class="flex items-center mb-1-rem">
		<label for="alpha3_code" class="w-20-%"><?php _e( 'Alpha3 Code', 'countries' ); ?></label>
		<input type="text" readonly name="alpha3_code" id="alpha3_code" class="w-80-%" value="<?php echo esc_attr( $alpha3_code ); ?>" />
	</div>

	<!-- Show Calling Codes Field -->
	<div class="flex items-center mb-1-rem">
		<label for="calling_codes" class="w-20-%"><?php _e( 'Calling Codes', 'countries' ); ?></label>
		<input type="text" readonly name="calling_codes" id="calling_codes" class="w-80-%"  value="<?php echo esc_attr( $calling_codes ); ?>" />
	</div>

	<!-- Show TimeZones Field -->
	<div class="flex items-center mb-1-rem">
		<label for="timezones" class="w-20-%"><?php _e( 'Timezones', 'countries' ); ?></label>
		<input type="text" readonly name="timezones" id="timezones" class="w-80-%" value="<?php echo esc_attr( $timezones ); ?>" />
	</div>

	<!-- Show Currencies -->
	<div class="flex items-center mb-1-rem">
		<label for="currencies" class="w-20-%"><?php _e( 'Currencies', 'countries' ); ?></label>
		<input type="text" readonly name="currencies" id="currencies" class="w-80-%" value="<?php echo esc_attr( $currencies ); ?>" />
	</div>

	<!-- Show Country Flag -->
	<div class="flex items-center mb-1-rem">
		<label for="country_flag" class="w-20-%"><?php _e( 'Flag', 'countries' ); ?></label>
		<img id="country_flag_image" class="w-200-px" src="<?php echo esc_html( $country_flag ); ?>"/>
		<input type="hidden" readonly name="country_flag" id="country_flag"  value="<?php echo esc_attr( $country_flag ); ?>" />
	</div>

	<!-- Show Country Publishin Time -->
	<div class="flex items-center mb-1-rem">
		<label for="country_publishing_time" class="w-20-%"><?php _e( 'Country Publishing Time', 'countries' ); ?></label>
		<input type="text" readonly id="country_publishing_time"  value="<?php echo esc_attr( $country_publishing_time ); ?>" />
	</div>
</div>

