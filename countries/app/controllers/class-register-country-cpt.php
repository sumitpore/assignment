<?php
namespace Countries\App\Controllers;

use Countries\Core\Controller;

class Register_Country_Cpt extends Controller {

	const CPT_SLUG = 'country';
	/**
	 * Registers Country CPT
	 *
	 * Made this method static because it is going to be needed in the plugin
	 * activator method while flushing rewrite rules.
	 *
	 * @return void
	 */
	public static function register() {
		$labels = array(
			'name'               => _x( 'Countries', 'post type general name', 'countries' ),
			'singular_name'      => _x( 'Country', 'post type singular name', 'countries' ),
			'menu_name'          => _x( 'Countries', 'admin menu', 'countries' ),
			'name_admin_bar'     => _x( 'Country', 'add new on admin bar', 'countries' ),
			'add_new'            => _x( 'Add New', self::CPT_SLUG, 'countries' ),
			'add_new_item'       => __( 'Add New Country', 'countries' ),
			'new_item'           => __( 'New Country', 'countries' ),
			'edit_item'          => __( 'Edit Country', 'countries' ),
			'view_item'          => __( 'View Country', 'countries' ),
			'all_items'          => __( 'All Countries', 'countries' ),
			'search_items'       => __( 'Search Countries', 'countries' ),
			'parent_item_colon'  => __( 'Parent Countries:', 'countries' ),
			'not_found'          => __( 'No countries found.', 'countries' ),
			'not_found_in_trash' => __( 'No countries found in Trash.', 'countries' ),
		);

		$args = array(
			'labels'             => $labels,
			'public'             => false,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => false,
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title' ),
		);

		register_post_type( self::CPT_SLUG, $args );
	}
}
