<?php
namespace Countries\App\Controllers\Admin;

use Countries;
use Countries\App\Controllers\Admin\Base_Controller;
use Countries\App\Controllers\Register_Country_Cpt;


class Country_Cpt extends Base_Controller {

	public function register_hook_callbacks() {
		add_filter( 'post_updated_messages', [ $this, 'cpt_messages' ] );

		// Enqueues scripts required on Add/Edit Country
		add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_scripts' ] );

		// Enqueues styles required on Add/Edit Country
		add_action( 'admin_enqueue_scripts', [ $this, 'enqueue_styles' ] );

		// Add Meta Boxes
		add_action( 'add_meta_boxes', [ $this, 'add_meta_boxes' ] );

		// Save Meta Boxes
		add_action( 'save_post', [ $this, 'save_meta_boxes' ] );
	}

	/**
	 * Country update messages.
	 *
	 * See /wp-admin/edit-form-advanced.php
	 *
	 * @param array $messages Existing post update messages.
	 *
	 * @return array Amended post update messages with new CPT update messages.
	 */
	public function cpt_messages( $messages ) {
		$post             = get_post();
		$post_type        = get_post_type( $post );
		$post_type_object = get_post_type_object( $post_type );

		$messages[ Register_Country_Cpt::CPT_SLUG ] = array(
			0  => '', // Unused. Messages start at index 1.
			1  => __( 'Country updated.', 'countries' ),
			2  => __( 'Custom field updated.', 'countries' ),
			3  => __( 'Custom field deleted.', 'countries' ),
			4  => __( 'Country updated.', 'countries' ),
			/* translators: %s: date and time of the revision */
			5  => isset( $_GET['revision'] ) ? sprintf( __( 'Country restored to revision from %s', 'countries' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
			6  => __( 'Country published.', 'countries' ),
			7  => __( 'Country saved.', 'countries' ),
			8  => __( 'Country submitted.', 'countries' ),
			9  => sprintf(
				__( 'Country scheduled for: <strong>%1$s</strong>.', 'countries' ),
				// translators: Publish box date format, see http://php.net/date
				date_i18n( __( 'M j, Y @ G:i', 'countries' ), strtotime( $post->post_date ) )
			),
			10 => __( 'Country draft updated.', 'countries' ),
		);

		if ( $post_type_object->publicly_queryable && Register_Country_Cpt::CPT_SLUG === $post_type ) {
			$permalink = get_permalink( $post->ID );

			$view_link                  = sprintf( ' <a href="%s">%s</a>', esc_url( $permalink ), __( 'View country', 'countries' ) );
			$messages[ $post_type ][1] .= $view_link;
			$messages[ $post_type ][6] .= $view_link;
			$messages[ $post_type ][9] .= $view_link;

			$preview_permalink           = add_query_arg( 'preview', 'true', $permalink );
			$preview_link                = sprintf( ' <a target="_blank" href="%s">%s</a>', esc_url( $preview_permalink ), __( 'Preview country', 'countries' ) );
			$messages[ $post_type ][8]  .= $preview_link;
			$messages[ $post_type ][10] .= $preview_link;
		}

		return $messages;
	}

	/**
	 * Enqueues Scripts required on Add/Edit Country Page
	 *
	 * @param string $hook
	 * @return void
	 */
	public function enqueue_scripts( $hook ) {
		if ( 'post.php' !== $hook && 'post-new.php' !== $hook ) {
			return;
		}

		if ( get_post_type() == Register_Country_Cpt::CPT_SLUG ) {
			wp_enqueue_script(
				Countries::PLUGIN_ID . '_cpt-js',
				Countries::get_plugin_url() . 'assets/js/admin/countries.js',
				array( 'jquery' ),
				Countries::PLUGIN_VERSION,
				true
			);

			wp_localize_script(
				Countries::PLUGIN_ID . '_cpt-js',
				'countries_plugin',
				[
					'all_countries' => $this->get_model()->get_all_countries(),
				]
			);
		}
	}

	/**
	 * Enqueues Styles required on Add/Edit Country Page
	 *
	 * @param string $hook
	 * @return void
	 */
	public function enqueue_styles( $hook ) {
		if ( 'post.php' !== $hook && 'post-new.php' !== $hook ) {
			return;
		}

		if ( get_post_type() == Register_Country_Cpt::CPT_SLUG ) {
			wp_enqueue_style(
				Countries::PLUGIN_ID . '_cpt-css',
				Countries::get_plugin_url() . 'assets/css/admin/countries.css',
				false,
				Countries::PLUGIN_VERSION
			);
		}
	}

	/**
	 * Add Meta Boxes on Country's Add/Edit Page
	 *
	 * @return void
	 */
	public function add_meta_boxes() {
		add_meta_box(
			'country_information',
			__( 'Country Information', 'countries' ),
			[ $this, 'country_information_html' ],
			'country',
			'normal',
			'high'
		);
		remove_meta_box( 'slugdiv', 'country', 'normal' );
	}

	/**
	 * Save Meta Boxes Values
	 *
	 * @param int $post_id
	 * @return void
	 */
	public function save_meta_boxes( $post_id ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if ( ! isset( $_POST['country_information_nonce'] ) || ! wp_verify_nonce( $_POST['country_information_nonce'], '_country_information_nonce' ) ) {
			return;
		}

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}

		// Set Post Id to be used in Model
		$this->get_model()->set_post_id( $post_id );

		if ( ! empty( $_POST['country_name'] ) ) {
			$this->get_model()->save_country_name( $_POST['country_name'] );
		}

		if ( ! empty( $_POST['top_level_domain'] ) ) {
			$this->get_model()->save_top_level_domain( $_POST['top_level_domain'] );
		}

		if ( ! empty( $_POST['alpha2_code'] ) ) {
			$this->get_model()->save_alpha2_code( $_POST['alpha2_code'] );
		}

		if ( ! empty( $_POST['alpha3_code'] ) ) {
			$this->get_model()->save_alpha3_code( $_POST['alpha3_code'] );
		}

		if ( ! empty( $_POST['calling_codes'] ) ) {
			$this->get_model()->save_calling_codes( $_POST['calling_codes'] );
		}

		if ( ! empty( $_POST['timezones'] ) ) {
			$this->get_model()->save_timezones( $_POST['timezones'] );
		}

		if ( ! empty( $_POST['currencies'] ) ) {
			$this->get_model()->save_currencies( $_POST['currencies'] );
		}

		if ( ! empty( $_POST['country_flag'] ) ) {
			$this->get_model()->save_country_flag( $_POST['country_flag'] );
		}
	}

	/**
	 * Show HTML of Meta Boxes Fields
	 *
	 * @param object $post
	 * @return void
	 */
	public function country_information_html( $post ) {

		// Set Post Id to be used in Model
		$this->get_model()->set_post_id( $post->ID );

		wp_nonce_field( '_country_information_nonce', 'country_information_nonce' );

		echo $this->get_view()->render_template(
			'admin/country-cpt-metaboxes.php', [
				'country_names'           => $this->get_model()->acceptable_country_names(),
				'country_name'            => $this->get_model()->country_name(),
				'top_level_domain'        => $this->get_model()->top_level_domain(),
				'alpha2_code'             => $this->get_model()->alpha2_code(),
				'alpha3_code'             => $this->get_model()->alpha3_code(),
				'calling_codes'           => $this->get_model()->calling_codes(),
				'timezones'               => $this->get_model()->timezones(),
				'currencies'              => $this->get_model()->currencies(),
				'country_flag'            => $this->get_model()->country_flag(),
				'country_publishing_time' => $this->get_model()->country_publishing_time(),
			]
		);
	}

}
