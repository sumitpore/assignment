<?php
namespace Countries\App\Models\Admin;

use Countries\App\Models\Admin\Base_Model;
use Countries\App\Models\Admin\Countries_Resources\Restcountries_Eu;

class Country_Cpt extends Base_Model {

	private $post_id       = null;
	private $countries_adapter = null;

	public function __construct() {
		// Fetch Countries from API
		$this->countries_adapter   = new Restcountries_Eu();
	}

	/**
	 * Sets Post Id to be used across all methods of this model
	 *
	 * @param int $post_id
	 * @return void
	 */
	public function set_post_id( $post_id ) {
		$this->post_id = $post_id;
	}

	/**
	 * Fetch All Countries from Remote Resource
	 *
	 * @return array
	 */
	public function get_all_countries() {
		return $this->countries_adapter->get_all_countries();
	}

	/**
	 * Returns List of All Country Names
	 *
	 * @return array
	 */
	public function country_names() {
		return $this->countries_adapter->get_all_country_names();
	}

	/**
	 * Returns Country Names Acceptable Current Country being created
	 *
	 * @return array
	 */
	public function acceptable_country_names() {
		global $wpdb;
		$countries_saved_in_db = $wpdb->get_col(
			$wpdb->prepare( "SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = %s AND meta_value != %s", '_country_name', $this->country_name() )
		);

		if ( $countries_saved_in_db ) {
			return array_diff( $this->country_names(), $countries_saved_in_db );
		}

		return $this->country_names();
	}

	/**
	 * Returns Country Name of the current country
	 *
	 * @return string
	 */
	public function country_name() {
		if ( ! empty( $this->post_id ) ) {
			return get_post_meta( $this->post_id, '_country_name', true );
		}

		return '';
	}

	/**
	 * Save Country Name of the current country
	 *
	 * @param string $country_name
	 * @return void
	 */
	public function save_country_name( $country_name ) {
		update_post_meta( $this->post_id, '_country_name', $country_name );
	}

	/**
	 * Returns Top Level Domain of the current Country Post
	 *
	 * @return string
	 */
	public function top_level_domain() {
		if ( ! empty( $this->post_id ) ) {
			return get_post_meta( $this->post_id, '_top_level_domain', true );
		}

		return '';
	}

	/**
	 * Save Top Level Domain of the current country
	 *
	 * @param string $top_level_domain
	 * @return void
	 */
	public function save_top_level_domain( $top_level_domain ) {
		update_post_meta( $this->post_id, '_top_level_domain', $top_level_domain );
	}

	/**
	 * Returns Alpha2 Code
	 *
	 * @return string
	 */
	public function alpha2_code() {
		if ( ! empty( $this->post_id ) ) {
			return get_post_meta( $this->post_id, '_alpha2_code', true );
		}

		return '';
	}

	/**
	 * Save Alpha2 Code of the current country
	 *
	 * @param string $alpha2_code
	 * @return void
	 */
	public function save_alpha2_code( $alpha2_code ) {
		update_post_meta( $this->post_id, '_alpha2_code', $alpha2_code );
	}

	/**
	 * Returns Alpha3 Code of current country
	 *
	 * @return string
	 */
	public function alpha3_code() {
		if ( ! empty( $this->post_id ) ) {
			return get_post_meta( $this->post_id, '_alpha3_code', true );
		}

		return '';
	}

	/**
	 * Save Alpha3 Code of the current country
	 *
	 * @param string $alpha3_code
	 * @return void
	 */
	public function save_alpha3_code( $alpha3_code ) {
		update_post_meta( $this->post_id, '_alpha3_code', $alpha3_code );
	}

	/**
	 * Returns Calling Codes of current country
	 *
	 * @return string
	 */
	public function calling_codes() {
		if ( ! empty( $this->post_id ) ) {
			return get_post_meta( $this->post_id, '_calling_codes', true );
		}

		return '';
	}

	/**
	 * Saves Calling Codes of current country
	 *
	 * @param string $calling_codes
	 * @return void
	 */
	public function save_calling_codes( $calling_codes ) {
		update_post_meta( $this->post_id, '_calling_codes', $calling_codes );
	}


	/**
	 * Return Timezones of current country
	 *
	 * @return string
	 */
	public function timezones() {
		if ( ! empty( $this->post_id ) ) {
			return get_post_meta( $this->post_id, '_timezones', true );
		}

		return '';
	}

	/**
	 * Saves Timezones for Current Country
	 *
	 * @param string $timezones
	 * @return void
	 */
	public function save_timezones( $timezones ) {
		update_post_meta( $this->post_id, '_timezones', $timezones );
	}

	/**
	 * Returns Currencies stored for the current country
	 *
	 * @return string
	 */
	public function currencies() {
		if ( ! empty( $this->post_id ) ) {
			return get_post_meta( $this->post_id, '_currencies', true );
		}

		return '';
	}

	/**
	 * Save Currencies of the current country
	 *
	 * @param string $currencies
	 * @return void
	 */
	public function save_currencies( $currencies ) {
		update_post_meta( $this->post_id, '_currencies', $currencies );
	}

	/**
	 * Returns Country Flag Url of the current country
	 *
	 * @return string
	 */
	public function country_flag() {
		if ( ! empty( $this->post_id ) ) {
			return get_post_meta( $this->post_id, '_country_flag', true );
		}

		return '';
	}

	/**
	 * Save Country Flag Url for current country
	 *
	 * @param string $country_flag
	 * @return void
	 */
	public function save_country_flag( $country_flag ) {
		update_post_meta( $this->post_id, '_country_flag', $country_flag );
	}

	/**
	 * Returns Publishing Time in current country's timezone
	 *
	 * @return string
	 */
	public function country_publishing_time() {

		$country_timezones = $this->timezones();

		if ( empty( $country_timezones ) ) {
			return '';
		}

		if ( is_string( $country_timezones ) ) {
			$country_timezone = $country_timezones;
		} else {
			$country_timezone = $country_timezones[0];
		}

		$country_timezone = str_replace( 'UTC', 'GMT', $country_timezone );
		$gmt_time         = get_post_modified_time( 'F d, Y g:i a', true, $this->post_id );
		$date_time        = new \DateTime( $gmt_time, new \DateTimeZone( 'GMT' ) );
		$offset           = ( new \DateTimeZone( $country_timezone ) )->getOffset( $date_time );
		$interval         = \DateInterval::createFromDateString( (string) $offset . 'seconds' );
		$date_time->add( $interval );
		return $date_time->format( 'F d, Y g:i a' );
	}
}
