<?php

namespace Countries\App\Models\Admin\Countries_Resources;

use Countries\App\Models\Admin\Countries_Resources\Adapter;

class Restcountries_Eu extends Adapter {
	private static $countries = [];

	/**
	 * @inheritDoc
	 */
	public function get_all_countries(): array {

		if ( empty( static::$countries ) ) {
			$this->fetch_countries();
		}

		return static::$countries;
	}

	/**
	 * Fetches countries from https://restcountries.eu/rest/v2/all
	 *
	 * @return void
	 */
	private function fetch_countries() {
		$countries   = [];
		$api_request = wp_remote_get( 'https://restcountries.eu/rest/v2/all' );

		if ( is_array( $api_request ) && ! is_wp_error( $api_request ) ) {
			$countries_returned_in_api = json_decode( $api_request['body'] ); // use the content
		}

		foreach ( $countries_returned_in_api as $country ) {

			$currencies = [];

			foreach ( $country->currencies as $currency ) {
				$currencies[] = $currency->name . ' ( Symbol: ' . $currency->symbol . ', Code: ' . $currency->code . ')';
			}

			$countries[ $country->name ] = [
				'country_name'     => $country->name,
				'top_level_domain' => $country->topLevelDomain,
				'alpha2_code'      => $country->alpha2Code,
				'alpha3_code'      => $country->alpha3Code,
				'calling_codes'    => $country->callingCodes,
				'timezones'        => $country->timezones,
				'currencies'       => $currencies,
				'country_flag'     => $country->flag,
			];
		}

		static::$countries = $countries;
	}

	/**
	 * @inheritDoc
	 */
	public function get_all_country_names(): array {

		if ( empty( static::$countries ) ) {
			$this->fetch_countries();
		}

		return array_keys( static::$countries );
	}


}
