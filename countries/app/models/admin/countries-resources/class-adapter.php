<?php
namespace Countries\App\Models\Admin\Countries_Resources;

abstract class Adapter {
	/**
	 * Fetch All Countries from Remote Resource
	 *
	 * @return array
	 */
	abstract public function get_all_countries(): array;

	/**
	 * Return List of All Country Names available
	 *
	 * @return array
	 */
	abstract public function get_all_country_names(): array;
}
