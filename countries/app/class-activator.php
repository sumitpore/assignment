<?php
namespace Countries\App;

use Countries\App\Controllers\Register_Country_Cpt;
use Countries;
/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Countries
 * @subpackage Countries/App
 * @author     Sumit P <sumit.pore@gmail.com>
 */
class Activator {

	const DB_VERSION = '1.0.0';

	/**
	 * Process to run when plugin is being activated.
	 *
	 * @since    1.0.0
	 */
	public function activate() {
		$this->flush_rewrite_rules();
	}

	private function flush_rewrite_rules() {
		Register_Country_Cpt::register();
		flush_rewrite_rules();
	}

}
