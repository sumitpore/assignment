<?php
namespace Countries\App;
use Countries\App\Controllers\Register_Country_Cpt;

/**
 * Fired during plugin uninstallation.
 *
 * This class defines all code necessary to run during the plugin's uninstallation.
 *
 * @since      1.0.0
 * @package    Countries
 * @subpackage Countries/App
 * @author     Sumit P <sumit.pore@gmail.com>
 */
class Uninstaller {

	/**
	 * Delete all countries.
	 *
	 * @since    1.0.0
	 */
	public function uninstall() {
		$countries = get_posts([
			'post_type'	=>	Register_Country_Cpt::CPT_SLUG,
			'numberposts' => -1
		]);

		if( $countries ){
			foreach( $countries as $country ){
				wp_delete_post($country->ID);
			}
		}
	}

}
