<?php

use Countries\Core\Route_Type as Route_Type;
use Countries\App\Controllers\Register_Country_Cpt;
use Countries\App\Controllers\Admin\Country_Cpt as Country_Cpt_Controller;
use Countries\App\Models\Admin\Country_Cpt as Country_Cpt_Model;
/*
|---------------------------------------------------------------------------------------------------------
| Available Route Types:-
|
| NOTE - Except Late Frontend Routes, all other routes are triggerred on 'init' hook.
|
+----------------------------------------------+---------------------------------------------------------+
| ROUTE TYPE                                   | ROUTE DESCRIPTION                                       |
+----------------------------------------------+---------------------------------------------------------+
| ROUTE_TYPE::ANY                              | To be used if model/controller is                       |
|                                              | required on all pages admin as well as frontend         |
+----------------------------------------------+---------------------------------------------------------+
| ROUTE_TYPE::ADMIN                            | To be used if model/controller needs to be loaded on    |
|                                              | on admin pages only                                     |
+----------------------------------------------+---------------------------------------------------------+
| ROUTE_TYPE::ADMIN_WITH_POSSIBLE_AJAX         | To be used if model/controller contains Ajax & needs    |
|                                              | to be loaded on admin pages only                        |
+----------------------------------------------+---------------------------------------------------------+
| ROUTE_TYPE::AJAX                             | To be used if model/controller contains Ajax            |
+----------------------------------------------+---------------------------------------------------------+
| ROUTE_TYPE::CRON                             | To be used if model/controller contains Cron            |
|                                              | functionality                                           |
+----------------------------------------------+---------------------------------------------------------+
| ROUTE_TYPE::FRONTEND                         | To be used if model/controller needs to be loaded on    |
|                                              | frontend pages only                                     |
+----------------------------------------------+---------------------------------------------------------+
| ROUTE_TYPE::FRONTEND_WITH_POSSIBLE_AJAX      | To be used if model/controller contains Ajax & needs    |
|                                              | to be loaded on frontend pages only                     |
+----------------------------------------------+---------------------------------------------------------+
| ROUTE_TYPE::LATE_FRONTEND                    | To be used if model/controller needs to be loaded when  |
|                                              | specific conditions are matched                         |
+----------------------------------------------+---------------------------------------------------------+
| ROUTE_TYPE::LATE_FRONTEND_WITH_POSSIBLE_AJAX | To be used if model/controller contains Ajax & needs    |
|                                              | to be loaded when specific conditions are matched       |
+----------------------------------------------+---------------------------------------------------------+
*
* Possible Routes Combinations :-
*
* 1. $router->register_route_of_type(...)->with_controller(...)->with_model(...)->with_view(...);
* 2. $router->register_route_of_type(...)->with_controller(...)->with_model(...);
* 3. $router->register_route_of_type(...)->with_controller(...)->with_view(...);
* 4. $router->register_route_of_type(...)->with_controller(...);
* 5. $router->register_route_of_type(...)->with_just_model(...);
*
* with_controller, with_model, with_view, with_just_model methods accept either a string or
* a callback. But the callback must return respective Controller/Model or View name.
*
* with_controlller & with_just_model methods supports '@' in the Controller/Model passed to
* them allowing you to call a particular method. That method does not need to be a static
* method. It can be a public method.
*/

/* That's all, start creating your own routes below this line! Happy coding. */

$router->register_route_of_type( ROUTE_TYPE::ANY )->with_controller( Register_Country_Cpt::class . '@register' );
$router->register_route_of_type( ROUTE_TYPE::ADMIN )->with_controller( Country_Cpt_Controller::class . '@register_hook_callbacks' )->with_model( Country_Cpt_Model::class );
