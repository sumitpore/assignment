<?php
namespace Countries\Core\Registry;

if ( ! class_exists( __NAMESPACE__ . '\\' . 'Model' ) ) {
	/**
	 * Model Registry
	 *
	 * Maintains the list of all models objects
	 *
	 * @since      1.0.0
	 * @package    Countries
	 * @subpackage Countries/Core/Registry
	 * @author     Sumit P <sumit.pore@gmail.com>
	 */
	class Model {
		use Base_Registry;
	}
}
