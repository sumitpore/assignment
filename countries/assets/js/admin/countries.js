(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note that this assume you're going to use jQuery, so it prepares
	 * the $ function reference to be used within the scope of this
	 * function.
	 *
	 * From here, you're able to define handlers for when the DOM is
	 * ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * Or when the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and so on.
	 *
	 * Remember that ideally, we should not attach any more than a single DOM-ready or window-load handler
	 * for any particular page. Though other scripts in WordPress core, other plugins, and other themes may
	 * be doing this, we should try to minimize doing that in our own work.
	 */

   $('#country_name').change(function(){
      // Search for country in localized object
      const country = countries_plugin.all_countries[$(this).val()];

      // Fill all fields
      $.each(country, function(field, value){

        //Convert Array to comma separated string
        if(Array.isArray(value)){
          value = value.join(', ');
        }

        // Fill Fields
        $('#' + field).val(value);
      });

      // Show SVG Image
      $('#country_flag_image').attr('src', country.country_flag);

      // Update Title
      $('#title').val(country.country_name);
   });

})( jQuery );
