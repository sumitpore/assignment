API Index
=========

* [Countries](Countries.md)
    * Countries\Core
        * [View](Countries-Core-View.md)
        * Countries\Core\Registry
            * [Controller](Countries-Core-Registry-Controller.md)
            * [Model](Countries-Core-Registry-Model.md)
        * [Route_Type](Countries-Core-Route_Type.md)
        * [Controller](Countries-Core-Controller.md)
        * [Model](Countries-Core-Model.md)
        * [Router](Countries-Core-Router.md)
    * Countries\App
        * [Deactivator](Countries-App-Deactivator.md)
        * [Uninstaller](Countries-App-Uninstaller.md)
        * [Activator](Countries-App-Activator.md)
        * Countries\App\Controllers
            * [Register_Country_Cpt](Countries-App-Controllers-Register_Country_Cpt.md)
            * Countries\App\Controllers\Frontend
                * [Base_Controller](Countries-App-Controllers-Frontend-Base_Controller.md)
            * Countries\App\Controllers\Admin
                * [Country_Cpt](Countries-App-Controllers-Admin-Country_Cpt.md)
                * [Base_Controller](Countries-App-Controllers-Admin-Base_Controller.md)
        * Countries\App\Models
            * Countries\App\Models\Frontend
                * [Base_Model](Countries-App-Models-Frontend-Base_Model.md)
            * Countries\App\Models\Admin
                * [Base_Model](Countries-App-Models-Admin-Base_Model.md)
                * [Country_Cpt](Countries-App-Models-Admin-Country_Cpt.md)
                * Countries\App\Models\Admin\Countries_Resources
                    * [Adapter](Countries-App-Models-Admin-Countries_Resources-Adapter.md)
                    * [Restcountries_Eu](Countries-App-Models-Admin-Countries_Resources-Restcountries_Eu.md)
    * Countries\Includes
        * [Dependency_Loader](Countries-Includes-Dependency_Loader.md)
        * [Requirements_Checker](Countries-Includes-Requirements_Checker.md)
        * [i18n](Countries-Includes-i18n.md)

