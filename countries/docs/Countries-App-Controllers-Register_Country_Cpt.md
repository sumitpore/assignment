Countries\App\Controllers\Register_Country_Cpt
===============

Abstract class to define/implement base methods for all controller classes




* Class name: Register_Country_Cpt
* Namespace: Countries\App\Controllers
* Parent class: [Countries\Core\Controller](Countries-Core-Controller.md)



Constants
----------


### CPT_SLUG

    const CPT_SLUG = 'country'





Properties
----------


### $model

    protected Object $model

Holds Model object



* Visibility: **protected**


### $view

    protected Object $view

Holds View Object



* Visibility: **protected**


Methods
-------


### register

    void Countries\App\Controllers\Register_Country_Cpt::register()

Registers Country CPT

Made this method static because it is going to be needed in the plugin
activator method while flushing rewrite rules.

* Visibility: **public**
* This method is **static**.




### get_instance

    object Countries\Core\Controller::get_instance(mixed $model_class_name, mixed $view_class_name)

Provides access to a single instance of a module using the singleton pattern



* Visibility: **public**
* This method is **static**.
* This method is defined by [Countries\Core\Controller](Countries-Core-Controller.md)


#### Arguments
* $model_class_name **mixed** - &lt;p&gt;Model Class to be associated with the controller.&lt;/p&gt;
* $view_class_name **mixed** - &lt;p&gt;View Class to be associated with the controller.&lt;/p&gt;



### get_model

    object Countries\Core\Controller::get_model()

Get model.

In most of the cases, the model will be set as per routes in defined in routes.php.
So if you are not sure which model class is currently being used, search for the
current controller class name in the routes.php

* Visibility: **protected**
* This method is defined by [Countries\Core\Controller](Countries-Core-Controller.md)




### get_view

    object Countries\Core\Controller::get_view()

Get view

In most of the cases, the view will be set as per routes in defined in routes.php.
So if you are not sure which view class is currently being used, search for the
current controller class name in the routes.php

* Visibility: **protected**
* This method is defined by [Countries\Core\Controller](Countries-Core-Controller.md)




### set_model

    void Countries\Core\Controller::set_model(\Countries\Core\Model $model)

Sets the model to be used



* Visibility: **protected**
* This method is defined by [Countries\Core\Controller](Countries-Core-Controller.md)


#### Arguments
* $model **[Countries\Core\Model](Countries-Core-Model.md)** - &lt;p&gt;Model object to be associated with the current controller object.&lt;/p&gt;



### set_view

    void Countries\Core\Controller::set_view(\Countries\Core\View $view)

Sets the view to be used



* Visibility: **protected**
* This method is defined by [Countries\Core\Controller](Countries-Core-Controller.md)


#### Arguments
* $view **[Countries\Core\View](Countries-Core-View.md)** - &lt;p&gt;View object to be associated with the current controller object.&lt;/p&gt;



### __construct

    mixed Countries\Core\Controller::__construct(\Countries\Core\Model $model, mixed $view)

Constructor



* Visibility: **protected**
* This method is defined by [Countries\Core\Controller](Countries-Core-Controller.md)


#### Arguments
* $model **[Countries\Core\Model](Countries-Core-Model.md)** - &lt;p&gt;Model object to be used with current controller object.&lt;/p&gt;
* $view **mixed** - &lt;p&gt;View object to be used with current controller object. Otherwise false.&lt;/p&gt;



### init

    void Countries\Core\Controller::init(\Countries\Core\Model $model, mixed $view)

Sets Model & View to be used with current controller



* Visibility: **protected**
* This method is defined by [Countries\Core\Controller](Countries-Core-Controller.md)


#### Arguments
* $model **[Countries\Core\Model](Countries-Core-Model.md)** - &lt;p&gt;Model to be associated with this controller.&lt;/p&gt;
* $view **mixed** - &lt;p&gt;Either View/its child class object or False.&lt;/p&gt;


