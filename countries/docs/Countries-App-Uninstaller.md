Countries\App\Uninstaller
===============

Fired during plugin uninstallation.

This class defines all code necessary to run during the plugin's uninstallation.


* Class name: Uninstaller
* Namespace: Countries\App







Methods
-------


### uninstall

    mixed Countries\App\Uninstaller::uninstall()

Short Description. (use period)

Long Description.

* Visibility: **public**



