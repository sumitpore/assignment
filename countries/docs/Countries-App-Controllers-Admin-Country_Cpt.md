Countries\App\Controllers\Admin\Country_Cpt
===============

Blueprint for Admin related Controllers. All Admin Controllers should extend this Base_Controller




* Class name: Country_Cpt
* Namespace: Countries\App\Controllers\Admin
* Parent class: [Countries\App\Controllers\Admin\Base_Controller](Countries-App-Controllers-Admin-Base_Controller.md)





Properties
----------


### $model

    protected Object $model

Holds Model object



* Visibility: **protected**


### $view

    protected Object $view

Holds View Object



* Visibility: **protected**


Methods
-------


### register_hook_callbacks

    mixed Countries\App\Controllers\Admin\Base_Controller::register_hook_callbacks()

Register callbacks for actions and filters. Most of your add_action/add_filter
go into this method.

NOTE: register_hook_callbacks method is not called automatically. You
as a developer have to call this method where you see fit. For Example,
You may want to call this in constructor, if you feel hooks/filters
callbacks should be registered when the new instance of the class
is created.

The purpose of this method is to set the convention that first place to
find add_action/add_filter is register_hook_callbacks method.

* Visibility: **protected**
* This method is **abstract**.
* This method is defined by [Countries\App\Controllers\Admin\Base_Controller](Countries-App-Controllers-Admin-Base_Controller.md)




### cpt_messages

    array Countries\App\Controllers\Admin\Country_Cpt::cpt_messages(array $messages)

Country update messages.

See /wp-admin/edit-form-advanced.php

* Visibility: **public**


#### Arguments
* $messages **array** - &lt;p&gt;Existing post update messages.&lt;/p&gt;



### enqueue_scripts

    void Countries\App\Controllers\Admin\Country_Cpt::enqueue_scripts(string $hook)

Enqueues Scripts required on Add/Edit Country Page



* Visibility: **public**


#### Arguments
* $hook **string**



### enqueue_styles

    void Countries\App\Controllers\Admin\Country_Cpt::enqueue_styles(string $hook)

Enqueues Styles required on Add/Edit Country Page



* Visibility: **public**


#### Arguments
* $hook **string**



### add_meta_boxes

    void Countries\App\Controllers\Admin\Country_Cpt::add_meta_boxes()

Add Meta Boxes on Country's Add/Edit Page



* Visibility: **public**




### save_meta_boxes

    void Countries\App\Controllers\Admin\Country_Cpt::save_meta_boxes(integer $post_id)

Save Meta Boxes Values



* Visibility: **public**


#### Arguments
* $post_id **integer**



### country_information_html

    void Countries\App\Controllers\Admin\Country_Cpt::country_information_html(object $post)

Show HTML of Meta Boxes Fields



* Visibility: **public**


#### Arguments
* $post **object**



### get_instance

    object Countries\Core\Controller::get_instance(mixed $model_class_name, mixed $view_class_name)

Provides access to a single instance of a module using the singleton pattern



* Visibility: **public**
* This method is **static**.
* This method is defined by [Countries\Core\Controller](Countries-Core-Controller.md)


#### Arguments
* $model_class_name **mixed** - &lt;p&gt;Model Class to be associated with the controller.&lt;/p&gt;
* $view_class_name **mixed** - &lt;p&gt;View Class to be associated with the controller.&lt;/p&gt;



### get_model

    object Countries\Core\Controller::get_model()

Get model.

In most of the cases, the model will be set as per routes in defined in routes.php.
So if you are not sure which model class is currently being used, search for the
current controller class name in the routes.php

* Visibility: **protected**
* This method is defined by [Countries\Core\Controller](Countries-Core-Controller.md)




### get_view

    object Countries\Core\Controller::get_view()

Get view

In most of the cases, the view will be set as per routes in defined in routes.php.
So if you are not sure which view class is currently being used, search for the
current controller class name in the routes.php

* Visibility: **protected**
* This method is defined by [Countries\Core\Controller](Countries-Core-Controller.md)




### set_model

    void Countries\Core\Controller::set_model(\Countries\Core\Model $model)

Sets the model to be used



* Visibility: **protected**
* This method is defined by [Countries\Core\Controller](Countries-Core-Controller.md)


#### Arguments
* $model **[Countries\Core\Model](Countries-Core-Model.md)** - &lt;p&gt;Model object to be associated with the current controller object.&lt;/p&gt;



### set_view

    void Countries\Core\Controller::set_view(\Countries\Core\View $view)

Sets the view to be used



* Visibility: **protected**
* This method is defined by [Countries\Core\Controller](Countries-Core-Controller.md)


#### Arguments
* $view **[Countries\Core\View](Countries-Core-View.md)** - &lt;p&gt;View object to be associated with the current controller object.&lt;/p&gt;



### __construct

    mixed Countries\Core\Controller::__construct(\Countries\Core\Model $model, mixed $view)

Constructor



* Visibility: **protected**
* This method is defined by [Countries\Core\Controller](Countries-Core-Controller.md)


#### Arguments
* $model **[Countries\Core\Model](Countries-Core-Model.md)** - &lt;p&gt;Model object to be used with current controller object.&lt;/p&gt;
* $view **mixed** - &lt;p&gt;View object to be used with current controller object. Otherwise false.&lt;/p&gt;



### init

    void Countries\Core\Controller::init(\Countries\Core\Model $model, mixed $view)

Sets Model & View to be used with current controller



* Visibility: **protected**
* This method is defined by [Countries\Core\Controller](Countries-Core-Controller.md)


#### Arguments
* $model **[Countries\Core\Model](Countries-Core-Model.md)** - &lt;p&gt;Model to be associated with this controller.&lt;/p&gt;
* $view **mixed** - &lt;p&gt;Either View/its child class object or False.&lt;/p&gt;


