Countries\App\Models\Admin\Countries_Resources\Adapter
===============






* Class name: Adapter
* Namespace: Countries\App\Models\Admin\Countries_Resources
* This is an **abstract** class







Methods
-------


### get_all_countries

    array Countries\App\Models\Admin\Countries_Resources\Adapter::get_all_countries()

Fetch All Countries from Remote Resource



* Visibility: **public**
* This method is **abstract**.




### get_all_country_names

    array Countries\App\Models\Admin\Countries_Resources\Adapter::get_all_country_names()

Return List of All Country Names available



* Visibility: **public**
* This method is **abstract**.



