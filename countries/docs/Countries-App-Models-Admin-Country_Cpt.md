Countries\App\Models\Admin\Country_Cpt
===============

Blueprint for Admin related Models. All Admin Models should extend this Base_Model




* Class name: Country_Cpt
* Namespace: Countries\App\Models\Admin
* Parent class: [Countries\App\Models\Admin\Base_Model](Countries-App-Models-Admin-Base_Model.md)





Properties
----------


### $post_id

    private mixed $post_id = null





* Visibility: **private**


### $countries_adapter

    private mixed $countries_adapter = null





* Visibility: **private**


Methods
-------


### __construct

    mixed Countries\App\Models\Admin\Country_Cpt::__construct()





* Visibility: **public**




### set_post_id

    void Countries\App\Models\Admin\Country_Cpt::set_post_id(integer $post_id)

Sets Post Id to be used across all methods of this model



* Visibility: **public**


#### Arguments
* $post_id **integer**



### get_all_countries

    array Countries\App\Models\Admin\Country_Cpt::get_all_countries()

Fetch All Countries from Remote Resource



* Visibility: **public**




### country_names

    array Countries\App\Models\Admin\Country_Cpt::country_names()

Returns List of All Country Names



* Visibility: **public**




### acceptable_country_names

    array Countries\App\Models\Admin\Country_Cpt::acceptable_country_names()

Returns Country Names Acceptable Current Country being created



* Visibility: **public**




### country_name

    string Countries\App\Models\Admin\Country_Cpt::country_name()

Returns Country Name of the current country



* Visibility: **public**




### save_country_name

    void Countries\App\Models\Admin\Country_Cpt::save_country_name(string $country_name)

Save Country Name of the current country



* Visibility: **public**


#### Arguments
* $country_name **string**



### top_level_domain

    string Countries\App\Models\Admin\Country_Cpt::top_level_domain()

Returns Top Level Domain of the current Country Post



* Visibility: **public**




### save_top_level_domain

    void Countries\App\Models\Admin\Country_Cpt::save_top_level_domain(string $top_level_domain)

Save Top Level Domain of the current country



* Visibility: **public**


#### Arguments
* $top_level_domain **string**



### alpha2_code

    string Countries\App\Models\Admin\Country_Cpt::alpha2_code()

Returns Alpha2 Code



* Visibility: **public**




### save_alpha2_code

    void Countries\App\Models\Admin\Country_Cpt::save_alpha2_code(string $alpha2_code)

Save Alpha2 Code of the current country



* Visibility: **public**


#### Arguments
* $alpha2_code **string**



### alpha3_code

    string Countries\App\Models\Admin\Country_Cpt::alpha3_code()

Returns Alpha3 Code of current country



* Visibility: **public**




### save_alpha3_code

    void Countries\App\Models\Admin\Country_Cpt::save_alpha3_code(string $alpha3_code)

Save Alpha3 Code of the current country



* Visibility: **public**


#### Arguments
* $alpha3_code **string**



### calling_codes

    string Countries\App\Models\Admin\Country_Cpt::calling_codes()

Returns Calling Codes of current country



* Visibility: **public**




### save_calling_codes

    void Countries\App\Models\Admin\Country_Cpt::save_calling_codes(string $calling_codes)

Saves Calling Codes of current country



* Visibility: **public**


#### Arguments
* $calling_codes **string**



### timezones

    string Countries\App\Models\Admin\Country_Cpt::timezones()

Return Timezones of current country



* Visibility: **public**




### save_timezones

    void Countries\App\Models\Admin\Country_Cpt::save_timezones(string $timezones)

Saves Timezones for Current Country



* Visibility: **public**


#### Arguments
* $timezones **string**



### currencies

    string Countries\App\Models\Admin\Country_Cpt::currencies()

Returns Currencies stored for the current country



* Visibility: **public**




### save_currencies

    void Countries\App\Models\Admin\Country_Cpt::save_currencies(string $currencies)

Save Currencies of the current country



* Visibility: **public**


#### Arguments
* $currencies **string**



### country_flag

    string Countries\App\Models\Admin\Country_Cpt::country_flag()

Returns Country Flag Url of the current country



* Visibility: **public**




### save_country_flag

    void Countries\App\Models\Admin\Country_Cpt::save_country_flag(string $country_flag)

Save Country Flag Url for current country



* Visibility: **public**


#### Arguments
* $country_flag **string**



### country_publishing_time

    string Countries\App\Models\Admin\Country_Cpt::country_publishing_time()

Returns Publishing Time in current country's timezone



* Visibility: **public**




### register_hook_callbacks

    mixed Countries\App\Models\Admin\Base_Model::register_hook_callbacks()

Register callbacks for actions and filters. Most of your add_action/add_filter
go into this method.

NOTE: register_hook_callbacks method is not called automatically. You
as a developer have to call this method where you see fit. For Example,
You may want to call this in constructor, if you feel hooks/filters
callbacks should be registered when the new instance of the class
is created.

The purpose of this method is to set the convention that first place to
find add_action/add_filter is register_hook_callbacks method.

This method is not marked abstract because it may not be needed in every
model. Making it abstract would enforce every child class to implement
the method.

If I were you, I would define register_hook_callbacks method in the child
class when it is a 'Model only' route. This is not a rule, it
is just my opinion when I would define this method.

* Visibility: **protected**
* This method is defined by [Countries\App\Models\Admin\Base_Model](Countries-App-Models-Admin-Base_Model.md)




### get_instance

    object Countries\Core\Model::get_instance()

Provides access to a single instance of a module using the singleton pattern



* Visibility: **public**
* This method is **static**.
* This method is defined by [Countries\Core\Model](Countries-Core-Model.md)



