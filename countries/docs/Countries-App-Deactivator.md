Countries\App\Deactivator
===============

Fired during plugin deactivation.

This class defines all code necessary to run during the plugin's deactivation.


* Class name: Deactivator
* Namespace: Countries\App







Methods
-------


### deactivate

    mixed Countries\App\Deactivator::deactivate()

Short Description. (use period)

Long Description.

* Visibility: **public**



