Countries\Core\Model
===============

Abstract class to define/implement base methods for model classes




* Class name: Model
* Namespace: Countries\Core







Methods
-------


### get_instance

    object Countries\Core\Model::get_instance()

Provides access to a single instance of a module using the singleton pattern



* Visibility: **public**
* This method is **static**.



