Countries\App\Models\Admin\Countries_Resources\Restcountries_Eu
===============






* Class name: Restcountries_Eu
* Namespace: Countries\App\Models\Admin\Countries_Resources
* Parent class: [Countries\App\Models\Admin\Countries_Resources\Adapter](Countries-App-Models-Admin-Countries_Resources-Adapter.md)





Properties
----------


### $countries

    private mixed $countries = array()





* Visibility: **private**
* This property is **static**.


Methods
-------


### get_all_countries

    array Countries\App\Models\Admin\Countries_Resources\Adapter::get_all_countries()

Fetch All Countries from Remote Resource



* Visibility: **public**
* This method is **abstract**.
* This method is defined by [Countries\App\Models\Admin\Countries_Resources\Adapter](Countries-App-Models-Admin-Countries_Resources-Adapter.md)




### fetch_countries

    void Countries\App\Models\Admin\Countries_Resources\Restcountries_Eu::fetch_countries()

Fetches countries from https://restcountries.eu/rest/v2/all



* Visibility: **private**




### get_all_country_names

    array Countries\App\Models\Admin\Countries_Resources\Adapter::get_all_country_names()

Return List of All Country Names available



* Visibility: **public**
* This method is **abstract**.
* This method is defined by [Countries\App\Models\Admin\Countries_Resources\Adapter](Countries-App-Models-Admin-Countries_Resources-Adapter.md)



