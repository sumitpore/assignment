Countries\App\Activator
===============

Fired during plugin activation.

This class defines all code necessary to run during the plugin's activation.


* Class name: Activator
* Namespace: Countries\App



Constants
----------


### DB_VERSION

    const DB_VERSION = '1.0.0'







Methods
-------


### activate

    mixed Countries\App\Activator::activate()

Process to run when plugin is being activated.



* Visibility: **public**




### flush_rewrite_rules

    mixed Countries\App\Activator::flush_rewrite_rules()





* Visibility: **private**



