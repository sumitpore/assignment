<?php
/**
 * Check and setup theme's default settings
 *
 * @package bootstrap-theme
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'bootstrap_theme_setup_theme_default_settings' ) ) {
	function bootstrap_theme_setup_theme_default_settings() {

		// check if settings are set, if not set defaults.
		// Caution: DO NOT check existence using === always check with == .
		// Latest blog posts style.
		$bootstrap_theme_posts_index_style = get_theme_mod( 'bootstrap_theme_posts_index_style' );
		if ( '' == $bootstrap_theme_posts_index_style ) {
			set_theme_mod( 'bootstrap_theme_posts_index_style', 'default' );
		}

		// Sidebar position.
		$bootstrap_theme_sidebar_position = get_theme_mod( 'bootstrap_theme_sidebar_position' );
		if ( '' == $bootstrap_theme_sidebar_position ) {
			set_theme_mod( 'bootstrap_theme_sidebar_position', 'right' );
		}

		// Container width.
		$bootstrap_theme_container_type = get_theme_mod( 'bootstrap_theme_container_type' );
		if ( '' == $bootstrap_theme_container_type ) {
			set_theme_mod( 'bootstrap_theme_container_type', 'container' );
		}
	}
}
