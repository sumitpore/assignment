<?php
/**
 * Template Name: Last Published Post Layout
 *
 * This template can be used to override the default template and sidebar setup
 *
 * @package bootstrap-theme
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'bootstrap_theme_container_type' );
?>

<div class="wrapper" id="page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content">

		<div class="row">

			<div
				class="<?php if ( is_active_sidebar( 'right-sidebar' ) ) : ?>col-md-8<?php else : ?>col-md-12<?php endif; ?> content-area"
				id="primary">

				<main class="site-main" id="main" role="main">

					<?php
					global $post;
					$arguments_for_latest_post = [
						'numberposts'      => 1,
						'offset'           => 0,
						'category'         => 0,
						'orderby'          => 'post_date',
						'order'            => 'DESC',
						'post_type'        => 'post',
						'post_status'      => 'publish',
						'suppress_filters' => true,
					];

					$recent_post = get_posts( $arguments_for_latest_post ); ?>
					<?php if(isset($recent_post[0])) : $post = $recent_post[0]; setup_postdata($post); ?>

						<?php get_template_part( 'loop-templates/content', 'single' ); ?>
						<?php wp_reset_postdata(); ?>

					<?php endif; ?>

					<?php while ( have_posts() ) : the_post(); ?>

						<?php get_template_part( 'loop-templates/content', 'page' ); ?>

						<?php
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
						?>

					<?php endwhile; // end of the loop. ?>

				</main><!-- #main -->

			</div><!-- #primary -->

			<?php get_template_part( 'sidebar-templates/sidebar', 'right' ); ?>

		</div><!-- .row -->

	</div><!-- #content -->

</div><!-- #page-wrapper -->

<?php get_footer(); ?>
