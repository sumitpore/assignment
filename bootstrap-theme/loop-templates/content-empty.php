<?php
/**
 * Content empty partial template.
 *
 * @package bootstrap-theme
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

the_content();
