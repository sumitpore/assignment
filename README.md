# WP Assignment

## Setting up Virtualhost - wp-assignment.com
1. Assuming Apache is installed on the computer, create a file `wp-assignment.com.conf`
inside `/etc/apache2/sites-available/wp-assignment.com.conf`. 

Add following content in it
```apache
<VirtualHost *:80>
	ServerAdmin webmaster@localhost
	ServerName wp-assignment.com
	DocumentRoot /var/www/html/wp-assignment
	<Directory />
		AllowOverride All
	</Directory>
	<Directory /var/www/html/wp-assignment>
		Options Indexes FollowSymLinks MultiViews
		AllowOverride all
		Require all granted
	</Directory>
	ErrorLog /var/log/apache2/wp-assignment.com-error.log
	LogLevel error
	CustomLog /var/log/apache2/wp-assignment.com-access.log combined
</VirtualHost>
```

2. After creating the file, run command `sudo a2ensite wp-assignment.com.conf && sudo service apache2 reload`
3. Open `/etc/hosts` file and add following line in it
```hosts
127.0.0.1    wp-assignment.com 
```
## Installing WordPress using WP-CLI (Commands)
```
$ cd /var/www/html/wp-assignment
$ wp core download
$ wp core config --dbhost=localhost --dbname=wp_assignment --dbuser=root --dbpass=root
$ sudo chmod 644 wp-config.php
$ wp core install --url=wp-assignment.com --title="WP Assignment" --admin_name=admin --admin_password=admin --admin_email=sumit.pore@gmail.com
$ cd wp-content
$ mkdir uploads
$ sudo chgrp www-data uploads/
$ sudo chmod 775 uploads/
```

## Enabling Debugging in WordPress
Insert below lines BEFORE `/* That's all, stop editing! Happy blogging. */` in the wp-config.php file.
```php
// Enable WP_DEBUG mode
define( 'WP_DEBUG', true );

// Enable Debug logging to the /wp-content/debug.log file
define( 'WP_DEBUG_LOG', true );

// Disable display of errors and warnings
define( 'WP_DEBUG_DISPLAY', false );
@ini_set( 'display_errors', 0 );

// Use dev versions of core JS and CSS files (only needed if you are modifying these core files)
define( 'SCRIPT_DEBUG', true );
```
## Install Debugging Plugin
Install Query Monitor Plugin
```
$ wp plugin install query-monitor --activate
```

## Creating a Plugin 
I use this boilerplate to create a base plugin
```
$ wget -O boilerplate-generator.sh https://raw.githubusercontent.com/sumitpore/mvc-plugin-boilerplate-for-wordpress/master/boilerplate-generator.sh && bash boilerplate-generator.sh

```

## Minimum Plugin Requirements
- Requires PHP 7.0
- Requires WordPress 4.8 and Above

## Architecture of the plugin
- MVC Architecture is followed. 
	- Model takes care of savng and retrieving data from the database as well as remote resource.
	- View takes care of showing Meta boxes.
	- Controller Takes care of all other things like enqueuing CSS-JS, Passing POSTed data to Model, Fetching Data from Model and Passing to View etc. 
- Fetching `https://restcountries.eu/rest/v2/all` data through an adapter of type `Countries_Resources\Adapter`, so that in future if source of data needs to be changed, then only new adapter needs to be written. Implementation will not need any changes.
- On Country's Add/Edit screen, a request to fetch all country's data is triggerred. Received response is filtered and data which we are interested in is only kept. Further, this data is used by JavaScript to load the details of the country, when user selects the country from dropdown. The same data is used by PHP also while loading 'Edit Country' Page to detect which country was saved in the database.
- For CSS classes, Utility pattern is followed and I have tried to make them as expressive as possible, so that developer understands what that class does without even looking at the developer tools. Here is the css file used in the plugin `assets/css/admin/countries.css`. This Approach is taken from `Tailwind CSS` Framework.
- Title of the Country is updated, when Country is saved. This way, Country listing page shows the same Country which was selected by user.
- One Country can not be selected more than once. For example, if you have created a Country `India`, you can not create another `India`.
- Grunt File Takes care of Creating `pot` files.



## Plugin Folder Structure
```
.
├── app
│   ├── class-activator.php
│   ├── class-deactivator.php
│   ├── class-uninstaller.php
│   ├── controllers ---------------> Has All Controller Classes
│   │   ├── admin
│   │   │   ├── class-base-controller.php
│   │   │   └── class-country-cpt.php
│   │   ├── class-register-country-cpt.php
│   │   └── frontend
│   │       └── class-base-controller.php
│   ├── models ---------------> Has All Models
│   │   ├── admin
│   │   │   ├── class-base-model.php
│   │   │   ├── class-country-cpt.php
│   │   │   └── countries-resources
│   │   │       ├── class-adapter.php
│   │   │       └── class-restcountries-eu.php
│   │   └── frontend
│   │       └── class-base-model.php
│   ├── templates ---------------> Has All HTML Templates
│   │   └── admin
│   │       ├── country-cpt-metaboxes.php
│   │       └── errors
│   │           ├── admin-notice.php
│   │           └── requirements-error.php
├── assets
│   ├── css
│   │   ├── admin
│   │   │   └── countries.css
│   │   └── frontend
│   │       └── countries.css
│   └── js
│       └── admin
│           └── countries.js
├── core -----------> Core Framework
│   ├── class-controller.php
│   ├── class-model.php
│   ├── class-router.php
│   ├── class-route-type.php
│   ├── class-view.php
│   └── registry
│       ├── class-controller.php
│       ├── class-model.php
│       └── trait-base-registry.php
├── countries.php
├── docs ----> Markdown Files of Class Documentation
├── .editorconfig
├── Gruntfile.js
├── includes
│   ├── class-countries.php ----> Main Countries Class
│   ├── class-dependency-loader.php
│   ├── class-i18n.php
│   ├── class-requirements-checker.php
│   └── index.php
├── index.php
├── languages
│   └── countries.pot
├── LICENSE.txt
├── package.json
├── README.txt
├── requirements-config.php ---> Defines the Minimum Requirement for the plugin.
├── routes.php  -----> Has All Routes Used in the plugin
├── uninstall.php ---> Deletes All Post when Plugin is Deleted.
└── .vscode
    └── settings.json
```

## Architecture of the theme
Theme is based on the [`UnderStrap`](https://github.com/understrap/understrap) starter theme which is combination of Automattic´s _s theme and Bootstrap.

A new page template is created named `Last Published Post Layout` which shows last published post on a page using that template.

## Theme Folder Structure
```
├── 404.php
├── archive.php  ---> Archive Template
├── author.php   ---> Author Archive Template
├── .browserslistrc
├── CHANGELOG.md
├── comments.php
├── composer.json
├── composer.lock
├── css
│   ├── custom-editor-style.css
│   ├── custom-editor-style.min.css
│   ├── custom-editor-style.min.css.map
│   ├── .DS_Store
│   ├── theme.css
│   ├── theme.min.css
│   └── theme.min.css.map
├── .editorconfig
├── fonts
├── footer.php ---> Footer Template
├── functions.php ---> Main functions.php
├── .gitignore
├── global-templates
│   ├── hero.php
│   ├── left-sidebar-check.php
│   └── right-sidebar-check.php
├── gulpconfig.json
├── gulpfile.js
├── header.php ---> Main Header File
├── inc ----> Helper Functionality
│   ├── class-wp-bootstrap-navwalker.php
│   ├── custom-comments.php
│   ├── custom-header.php
│   ├── customizer.php
│   ├── deprecated.php
│   ├── .DS_Store
│   ├── editor.php
│   ├── enqueue.php
│   ├── extras.php
│   ├── hooks.php
│   ├── jetpack.php
│   ├── pagination.php
│   ├── setup.php
│   ├── style-wpcom.css
│   ├── template-tags.php
│   ├── theme-settings.php
│   ├── widgets.php
│   ├── woocommerce.php
│   └── wpcom.php
├── index.php
├── js
│   ├── customizer.js
│   ├── theme.js
│   └── theme.min.js
├── .jscsrc
├── .jshintignore
├── languages
├── LICENSE.md
├── loop-templates -----> WordPress Loop Templates
│   ├── content-blank.php
│   ├── content-empty.php
│   ├── content-none.php
│   ├── content-page.php
│   ├── content.php
│   ├── content-search.php
│   └── content-single.php
├── package.json
├── package-lock.json
├── page.php ----> Main Page Template
├── page-templates -----> Multiple Page Templates
│   ├── blank.php
│   ├── both-sidebarspage.php
│   ├── empty.php
│   ├── fullwidthpage.php
│   ├── left-sidebarpage.php
│   ├── published-post.php
│   └── right-sidebarpage.php
├── phpcs.xml
├── README.md
├── readme.txt
├── sass
│   ├── assets
│   │   ├── bootstrap4.scss
│   │   ├── font-awesome.scss
│   │   └── underscores.scss
│   ├── custom-editor-style.scss
│   ├── .DS_Store
│   ├── theme
│   │   ├── _contact-form7.scss
│   │   ├── _theme.scss
│   │   └── _theme_variables.scss
│   ├── theme.scss
│   └── understrap
│       ├── understrap.scss
│       └── woocommerce.scss
├── screenshot.png
├── searchform.php ----> Search Form
├── search.php -----> Search Page Template
├── sidebar.php ---> Main Sidebar template
├── sidebar-templates -----> Sidebar Templates
│   ├── sidebar-footerfull.php
│   ├── sidebar-herocanvas.php
│   ├── sidebar-hero.php
│   ├── sidebar-left.php
│   ├── sidebar-right.php
│   └── sidebar-statichero.php
├── single.php ---> Single Post Template
├── src
│   ├── js
│   │   ├── bootstrap4
│   │   │   ├── bootstrap.bundle.js
│   │   │   ├── bootstrap.bundle.min.js
│   │   │   ├── bootstrap.js
│   │   │   └── bootstrap.min.js
│   │   └── skip-link-focus-fix.js
│   └── sass
│       ├── bootstrap4 -----> Bootstrap Sass Files
│       │   ├── _alert.scss
│       │   ├── _badge.scss
│       │   ├── bootstrap-grid.scss
│       │   ├── bootstrap-reboot.scss
│       │   ├── bootstrap.scss
│       │   ├── _breadcrumb.scss
│       │   ├── _button-group.scss
│       │   ├── _buttons.scss
│       │   ├── _card.scss
│       │   ├── _carousel.scss
│       │   ├── _close.scss
│       │   ├── _code.scss
│       │   ├── _custom-forms.scss
│       │   ├── _dropdown.scss
│       │   ├── _forms.scss
│       │   ├── _functions.scss
│       │   ├── _grid.scss
│       │   ├── _images.scss
│       │   ├── _input-group.scss
│       │   ├── _jumbotron.scss
│       │   ├── _list-group.scss
│       │   ├── _media.scss
│       │   ├── mixins
│       │   │   ├── _alert.scss
│       │   │   ├── _background-variant.scss
│       │   │   ├── _badge.scss
│       │   │   ├── _border-radius.scss
│       │   │   ├── _box-shadow.scss
│       │   │   ├── _breakpoints.scss
│       │   │   ├── _buttons.scss
│       │   │   ├── _caret.scss
│       │   │   ├── _clearfix.scss
│       │   │   ├── _deprecate.scss
│       │   │   ├── _float.scss
│       │   │   ├── _forms.scss
│       │   │   ├── _gradients.scss
│       │   │   ├── _grid-framework.scss
│       │   │   ├── _grid.scss
│       │   │   ├── _hover.scss
│       │   │   ├── _image.scss
│       │   │   ├── _list-group.scss
│       │   │   ├── _lists.scss
│       │   │   ├── _nav-divider.scss
│       │   │   ├── _pagination.scss
│       │   │   ├── _reset-text.scss
│       │   │   ├── _resize.scss
│       │   │   ├── _screen-reader.scss
│       │   │   ├── _size.scss
│       │   │   ├── _table-row.scss
│       │   │   ├── _text-emphasis.scss
│       │   │   ├── _text-hide.scss
│       │   │   ├── _text-truncate.scss
│       │   │   ├── _transition.scss
│       │   │   └── _visibility.scss
│       │   ├── _mixins.scss
│       │   ├── _modal.scss
│       │   ├── _navbar.scss
│       │   ├── _nav.scss
│       │   ├── _pagination.scss
│       │   ├── _popover.scss
│       │   ├── _print.scss
│       │   ├── _progress.scss
│       │   ├── _reboot.scss
│       │   ├── _root.scss
│       │   ├── _spinners.scss
│       │   ├── _tables.scss
│       │   ├── _toasts.scss
│       │   ├── _tooltip.scss
│       │   ├── _transitions.scss
│       │   ├── _type.scss
│       │   ├── utilities
│       │   │   ├── _align.scss
│       │   │   ├── _background.scss
│       │   │   ├── _borders.scss
│       │   │   ├── _clearfix.scss
│       │   │   ├── _display.scss
│       │   │   ├── _embed.scss
│       │   │   ├── _flex.scss
│       │   │   ├── _float.scss
│       │   │   ├── _overflow.scss
│       │   │   ├── _position.scss
│       │   │   ├── _screenreaders.scss
│       │   │   ├── _shadows.scss
│       │   │   ├── _sizing.scss
│       │   │   ├── _spacing.scss
│       │   │   ├── _stretched-link.scss
│       │   │   ├── _text.scss
│       │   │   └── _visibility.scss
│       │   ├── _utilities.scss
│       │   ├── _variables.scss
│       │   └── vendor
│       │       └── _rfs.scss
│       ├── fontawesome
│       │   ├── _animated.scss
│       │   ├── _bordered-pulled.scss
│       │   ├── _core.scss
│       │   ├── _fixed-width.scss
│       │   ├── font-awesome.scss
│       │   ├── _icons.scss
│       │   ├── _larger.scss
│       │   ├── _list.scss
│       │   ├── _mixins.scss
│       │   ├── _path.scss
│       │   ├── _rotated-flipped.scss
│       │   ├── _screen-reader.scss
│       │   ├── _stacked.scss
│       │   └── _variables.scss
│       └── underscores
│           ├── _captions.scss
│           ├── _galleries.scss
│           └── _media.scss
├── style.css
├── .travis.yml
└── woocommerce ----> WooCommerce Related Templates
    ├── cart
    │   ├── cart-empty.php
    │   ├── cart.php
    │   ├── mini-cart.php
    │   └── proceed-to-checkout-button.php
    ├── checkout
    │   ├── form-checkout.php
    │   ├── form-coupon.php
    │   ├── form-pay.php
    │   └── payment.php
    ├── global
    │   ├── form-login.php
    │   └── quantity-input.php
    ├── loop
    │   ├── add-to-cart.php
    │   └── orderby.php
    ├── myaccount
    │   ├── downloads.php
    │   ├── form-edit-account.php
    │   ├── form-edit-address.php
    │   ├── form-login.php
    │   ├── form-lost-password.php
    │   ├── form-reset-password.php
    │   ├── my-address.php
    │   ├── my-orders.php
    │   ├── navigation.php
    │   └── orders.php
    ├── product-searchform.php
    └── single-product
        └── add-to-cart
            ├── simple.php
            └── variation-add-to-cart-button.php

```